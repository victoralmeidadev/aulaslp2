﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercicio_0503_1
{
    class Banco
    {
        private int numBanco;
        private List<Conta> contas = new List<Conta>();

        

        public int NumBanco { get; set; }

        internal List<Conta> Contas
        {
            get
            {
                return contas;
            }

            set
            {
                contas = value;
            }
        }

        public void ListarContas()
        {

        }

        public  void ListarContas(long numAgencia)
        {

        }

        public double AprovarLimite(double valor, int tempoConta)
        {
            return 0;
        }
    }
}
